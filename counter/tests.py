from django.test import TestCase, Client
from counter.models import Default
from django.utils import timezone
import logging

logger = logging.getLogger(__name__)


class IndexViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.default = Default.objects.create(default_number=333, pub_date=timezone.now())
        
    def test_index_post(self):
        response = self.client.post('/counter/', {'count': 10})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Default.objects.first().default_number, 10)
        self.assertEqual(response.json(), {'message': 'Count saved successfully.'})

    def test_index_get(self):
        with self.assertTemplateUsed('counter/index.html'):
            response = self.client.get('/counter/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Default.objects.first().default_number, 333)
