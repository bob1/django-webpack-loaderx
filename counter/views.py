from django.views import generic
from django.shortcuts import render
from .models import Default
from django.http import JsonResponse
import json
import logging

#initialize the logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG) 
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)

def index(request):
    if request.method == 'POST':
        return save_counter(request)
    else:
        return show_counter(request)

def save_counter(request):
    q = Default.objects.first()

    #get data from json
    data = json.loads(request.body)
    value = data.get('count') 
   
    if value:
        q.default_number = value
        logger.debug(value)
    else:
        q.default_number = None

    q.save()
    return JsonResponse({'message': 'Count saved successfully.'})

def show_counter(request):
    data_from_db = Default.objects.first().default_number
    context = {
        'default_number': data_from_db
    }
    return render(request, "counter/index.html", context)

