from django.db import models


class Default(models.Model):
    default_number = models.IntegerField()
    pub_date = models.DateTimeField("date published")
    def __str__(self):
        return str(self.default_number)