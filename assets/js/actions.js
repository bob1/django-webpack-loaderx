const testNumber = 2;
export const defaultNumber = window.defaultNumber || testNumber;
const csrfToken = window.csrfToken;

console.log(`defaultNumber: ${defaultNumber}`);
console.log(`CSRF Token: ${csrfToken}`);

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const SAVE = 'SAVE';
export const SUBMIT = 'SUBMIT';
export const POST_SUCCESS = 'POST_SUCCESS';
export const POST_ERROR = 'POST_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
export const SET_INPUT_VALUE = 'SET_INPUT_VALUE';

export const increment = () => ({ type: INCREMENT });
export const decrement = () => ({ type: DECREMENT });
export const submit = (inputValue) => ({ type: SUBMIT, payload: inputValue});

export const postSuccess = (message) => ({ type: POST_SUCCESS, payload: message });
export const postError = (message) => ({ type: POST_ERROR, payload: message });
export const resetMessage = () => ({ type: RESET_MESSAGE });
export const setInputValue = (inputValue) => ({ type: SET_INPUT_VALUE, payload: inputValue });

export const handleSave = () => {
	return (dispatch, getState) => {
		fetch('', {
		  method: 'POST',
		  credentials: 'include',
		     headers: {
        		'X-CSRFToken': csrfToken
        	},
		  body: JSON.stringify({count: getState().count})
		})
		  .then((response) => {
		    if (!response.ok) {
		      throw new Error('Post error!');
		    }
		    dispatch(postSuccess('Post successful!'));
		  })
		  .catch((error) => {
		    dispatch(postError('Post error!'));
		  });
	};
};

export const handleSubmit = (inputValue) => {
	return (dispatch) => {
		fetch('', {
		  method: 'POST',
		  credentials: 'include',
		     headers: {
        		'X-CSRFToken': csrfToken,
        	},
		  body: JSON.stringify({count: inputValue})
		})
		  .then((response) => {
		  	if (!response.ok) {
		      throw new Error('Post error!');
		    }
		    //refresh count number
		    dispatch(submit(inputValue));
		    dispatch(postSuccess('Post successful!'));
		  })
		  .catch((error) => {
		    dispatch(postError('Post error!'));
		  });
	};
};
