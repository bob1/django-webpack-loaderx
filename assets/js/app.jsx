import React from 'react';
import '../css/app.css';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import counterReducer from './reducer';
import Counter from './counter';
import thunk from 'redux-thunk';

const store = createStore(counterReducer, applyMiddleware(thunk));

const App = () => {
  return (
    <Provider store={store}>
      <Counter />
    </Provider>
  );
};

export default App;
