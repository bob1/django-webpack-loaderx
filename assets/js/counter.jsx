import React, { useEffect, useState} from 'react';
import { connect, useDispatch } from 'react-redux';
import * as actions from './actions';
import { SaveButton } from './saveButton';
import { SubmitForm } from './submitForm';
import { QuantityButton } from './quantityButton';

function hideMessage(Message, resetMessage) {
  const dispatch = useDispatch();

  useEffect(() => {
    const timer = setTimeout(() => {
      dispatch(resetMessage());
    }, 1000);

    return () => {
      clearTimeout(timer);
    };
  }, [Message]);
}

const Counter = ({ count, Message, increment, decrement, resetMessage, handleSave, handleSubmit, setInputValue, inputValue }) => {
  hideMessage(Message, resetMessage);

  return (
    <div className="container1">
      <h1>Count: {count}</h1>
      <div className="container2">
        <QuantityButton onClick={increment} icon='+' />
        <QuantityButton onClick={decrement} icon='-' />
        <SaveButton className='buttonstyle2' buttonName='Save' onClick={() => handleSave()}/>
      </div>
      <SubmitForm inputValue={inputValue} setInputValue={setInputValue} onClick={() => handleSubmit(inputValue)}/>
      {Message && <p>{Message}</p>}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    count: state.count,
    Message: state.Message,
    inputValue: state.inputValue
  };
};

const mapDispatchToProps = {
  increment: actions.increment,
  decrement: actions.decrement,
  resetMessage: actions.resetMessage,
  handleSave: actions.handleSave,
  handleSubmit: actions.handleSubmit,
  setInputValue: actions.setInputValue
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);