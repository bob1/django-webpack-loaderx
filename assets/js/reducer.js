import { defaultNumber } from './actions';

const initialState = {
  count: defaultNumber
};

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        count: state.count + 1,
      };
    case 'DECREMENT':
      return {
        ...state,
        count: Math.max(0, state.count - 1),
      }
    case 'SUBMIT':
      return {
        ...state,
        count: action.payload
      };
    case 'POST_SUCCESS':
      return {
        ...state,
        Message: action.payload,
      };
    case 'POST_ERROR':
      return {
        ...state,
        Message: action.payload,
      };
    case 'RESET_MESSAGE':
      return {
        ...state,
        Message: undefined,
      };
    case 'SET_INPUT_VALUE': 
      return {
        ...state,
        inputValue: action.payload,
      };
    default:
      return state;
  }
};

export default counterReducer;