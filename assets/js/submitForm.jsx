import React from 'react';
import { SaveButton } from './saveButton';

export function SubmitForm ({ inputValue, setInputValue, onClick }) {
  return (
    <div>
      <input 
        type="text" 
        name="count" 
        placeholder="Please input a number" 
        onChange={(event) => setInputValue(event.target.value)}
        value={inputValue}
      />
      <SaveButton buttonName='Submit' onClick={onClick}/>
    </div>
  );
};