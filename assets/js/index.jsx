import React from 'react';
import ReactDOM from 'react-dom';

import MyApp from './app';

ReactDOM.render(<MyApp/>, document.getElementById('react-app'));
