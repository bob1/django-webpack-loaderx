import React from 'react';

export function QuantityButton({ onClick, icon }) {
  return (
    <button className="buttonstyle1" onClick={onClick}>
      {icon}
    </button>
  );
}