import React from 'react';

export function SaveButton({ className, buttonName, onClick }) {
  return (
    <button className={className} onClick={onClick}>
      {buttonName}
    </button>
  );
}