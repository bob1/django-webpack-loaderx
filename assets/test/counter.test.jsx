import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Counter from '../js/counter';
import renderer, { act, create } from 'react-test-renderer'
import counterReducer from '../js/reducer';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import fetchMock from 'jest-fetch-mock';

fetchMock.enableMocks();
jest.useFakeTimers();

test('increment and decrement', () => {
  const store = createStore(counterReducer);

  const component = create(
    <Provider store={store}>
      <Counter />
    </Provider>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  let decrementButton;
  for (let i=0; i<3; i++) {
    decrementButton = component.root.findByProps({children: '-'});
    act(() => {
      decrementButton.props.onClick();
    })
  }
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  const incrementButton = component.root.findByProps({children: '+'});
  act(() => {
    incrementButton.props.onClick();
  })
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})

test('submit form', async () => {
  
  const store = createStore(counterReducer, applyMiddleware(thunk));
  const component = render(
    <Provider store={store}>
      <Counter />
    </Provider>
  );

  const { getByText, getByPlaceholderText, container } = component
  const submitButton = getByText('Submit');
  const input = getByPlaceholderText('Please input a number');

  fireEvent.change(input, { target: { value: '10' } });
  fireEvent.click(submitButton);

  //wait post request and update UI
  await waitFor(() => {
    expect(fetchMock.mock.calls.length).toBe(1);
  })
  //should have success message
  expect(container.innerHTML).toMatchSnapshot();

  //should hide success message after 1 second
  jest.advanceTimersByTime(1000);
  expect(container.innerHTML).toMatchSnapshot();

  fetchMock.mockClear();
})

test('save number', async () => {
  const store = createStore(counterReducer, applyMiddleware(thunk));
  const component = render(
    <Provider store={store}>
      <Counter />
    </Provider>
  );

  const { getByText, container } = component
  const decrementButton = getByText('-');
  const saveButton = getByText('Save');
  fireEvent.click(decrementButton);
  fireEvent.click(saveButton);
  await waitFor(() => {
    expect(fetchMock.mock.calls.length).toBe(1);
  })
  expect(container.innerHTML).toMatchSnapshot();

  jest.advanceTimersByTime(1000);
  expect(container.innerHTML).toMatchSnapshot();

  fetchMock.mockClear();
})


